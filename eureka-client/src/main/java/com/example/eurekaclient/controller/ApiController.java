package com.example.eurekaclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class ApiController {

    @Value("${eureka.instance.instance-id}")
    private String instance;


    @GetMapping("/ok")
    public String ok() {
        return String.format("[GET] api/v1/ok {%s}", instance);
    }

}
