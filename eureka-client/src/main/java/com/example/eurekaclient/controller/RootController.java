package com.example.eurekaclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class RootController {

    @Value("${eureka.instance.instance-id}")
    private String instance;

    @GetMapping("/root")
    public String root() {
        return String.format("[GET] /root {%s}", instance);
    }

    @GetMapping("/")
    public String main() {
        return String.format("[GET] / {%s}", instance);
    }

}
